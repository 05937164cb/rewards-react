import React, { Component } from 'react';
import consumables from "./images/Consumable_PCY009_CrashCrewPremium.png";
import currency from "./images/Wows_credits.png";
import specials from "./images/Icon_16.png";
import camouflages from "./images/PBEC503_Campbeltown.png";
import flags from "./images/PCEF020_Blue_Dragon_Flag.png";
import logo from "./images/wows_logo.png";

class Left extends Component {

  constructor(props){
    super(props);
    this.state = {categories: null,
                    el: '',
                    sel: '',
                    uid: props.uid,
                    contType: props.contType,
                    supr: props.superType
                  };
    this.searchFromSolr = this.searchFromSolr.bind(this);
  }

  componentDidMount(){
    let categories = this;
    let cats = null;
    let el = null;
    try{
      fetch("http://api.wows.rule10.eu/api/v1/getCategories")
      .then(response => response.json())
      .then(function(json){
        cats = json;
        categories.formatCats(json);
      })
    }catch(e){
      console.log("caught error on componentDidMount");
      cats = "ERROR";
    }
  }

  ucfirst(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  async searchFromSolr(e){
    let value = e.target.value;
    let searchUrl = "http://rule10.eu:8080/solr/rewardsUsers/select?q=username:*"+value+"*&rows=5&start=0&wt=json";
    let users = "";
    let usrBox = document.getElementById("userSearchResults");
    let supr = this.state.supr;
    let typ = this.state.contType;
    if(!supr){
      supr = 0;
    }
    if(!typ){
      typ = 1;
    }
    if(value.length > 1){
      try{
        await fetch(searchUrl)
        .then(response => response.json())
        .then(function(json){
          json.response.docs.forEach(entry => {
            let uname = entry.returnName[0];
            let id = entry.real_id[0];
            users += "<a href='/"+id+"/"+supr+"/"+typ+"'><div class='unameEntry'>"+uname+"</div></a>";
          });
        })
        if(users!==""){
          usrBox.classList.remove("hidden");
          usrBox.classList.add("shown");
          try{
            usrBox.innerHTML = users;
          }catch(e){
            console.log(e);
          }
        }else{
          if(!usrBox.classList.contains("hidden")){
            usrBox.classList.remove("shown");
            usrBox.classList.add("hidden");
          }
        }

      }catch(e){
        console.log("solr error");
        // do nothing
      }
      console.log(users);
    }else{
      if(!usrBox.classList.contains("hidden")){
        usrBox.classList.remove("shown");
        usrBox.classList.add("hidden");
      }
    }
  }

  getSearch(){
    return(
      <div className="search floatRight">
        <input type="text" placeholder="Search users" id="searchBox" ref="search" onKeyUp={this.searchFromSolr} />
        <div className="hidden" id="userSearchResults"></div>
      </div>
    )
  }

  getLogin(){
    return(
      <div className="login floatRight">
        Missing JWT implementation...
      </div>
    )
  }

  formatCats(arr){
    let el = null;
    let sel = null;
    let usr = this.state.uid;
    let dis = this;
    arr.result.forEach(val => {
      let icon = null;
      switch(val.name){
        case "consumables":
          icon = consumables;
          break;
        case "currency":
          icon = currency;
          break;
        case "specials":
          icon = specials;
          break;
        case "camouflages":
          icon = camouflages;
          break;
        case "flags":
          icon = flags;
          break;
        default:
          icon = null;
          break;
      }

      let selected = "";
      if(val.id.toString()===dis.state.contType){
        selected=" selected";
      }
      if(el===null || sel===null){
        el = "<a href='/"+usr+"/0/"+val.id+"'><div class='cates"+selected+"' id='cat_"+val.id+"'><img class='cat_icon' src='"+icon+"' /><br />"+this.ucfirst(val.name)+"</div></a>";
        sel = "<a href='/"+usr+"/1/"+val.id+"'><div class='cates' id='superCat_"+val.id+"'><img class='cat_icon' src='"+icon+"' /><br />"+this.ucfirst(val.name)+"</div></a>";
      }else{
        el += "<a href='/"+usr+"/0/"+val.id+"'><div class='cates"+selected+"' id='cat_"+val.id+"'><img class='cat_icon' src='"+icon+"' /><br />"+this.ucfirst(val.name)+"</div></a>";
        sel += "<a href='/"+usr+"/1/"+val.id+"'><div class='cates' id='superCat_"+val.id+"'><img class='cat_icon' src='"+icon+"' /><br />"+this.ucfirst(val.name)+"</div></a>";
      }
    });
    this.setState({el: <div className='cats' dangerouslySetInnerHTML={{__html: el}}></div>});
    this.setState({sel: <div className='superCats' dangerouslySetInnerHTML={{__html: sel}}></div>});
  }

  superSelect(uid = null, contType = null, supr = null){
    let el = null;
    if(uid!==null && contType!==null){
      let superIcon = require("./images/Icon_21.png");
      let selectedSuper = "";
      let selectedNormal = "";
      if(supr==="1"){
        selectedSuper = "selected";
      }else{
        selectedNormal = "selected";
      }
      el = "<a class='"+selectedSuper+"' href='/"+uid+"/1/"+contType+"'><img class='icon' src='"+superIcon+"' /><br />super</a> <a class='"+selectedNormal+"' href='/"+uid+"/0/"+contType+"'><span class='icon' style='font-size: 250%;'>N</span><br />normal</a>";
    }else{
      el = null;
    }
    return(el);
  }

  render() {
    if(!this.state.uid){
      return(
        <div className="pageLeft navi">
          <div className="logo">
            <img className='logo' src={logo} />
          </div>

          <div className="floatRight">
              {
                // this.state.sel
              }
              {this.getSearch()}
              {this.getLogin()}
          </div>
        </div>
      );
    }else{
      return (
        <div className="pageLeft navi">
          <div className="logo">
            <a href="/"><img className='logo' src={logo} /></a>
          </div>
          <div className="cats">
            {
              this.state.el
            }
            <div className="superSelector" dangerouslySetInnerHTML={{__html: this.superSelect(this.state.uid, this.state.contType, this.state.supr)}}></div>
          </div>
          <div className="floatRight">
              {
                // this.state.sel
              }
              {this.getSearch()}
              {this.getLogin()}
          </div>
        </div>
      );
    }
  }
}

export default Left;
