import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import Content from './App';
// import Left from "./left";
// import Right from "./right";
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

ReactDOM.render(<BrowserRouter>
                    <Switch>
                        <Route path='/:id/:supr/:type' component={Content} />
                        <Route path='/:id' component={Content} />
                        <Route path="/" component={Content} />
                    </Switch>
                </BrowserRouter>, document.getElementById('root'));
serviceWorker.unregister();
