import React, { Component } from 'react';

class Right extends Component {

  constructor(props){
    super(props);
    this.state = {
      uid: props.uid,
      uname: props.uname,
      loaded: false,
      allEntriesLoaded: false,
      normalEntriesLoaded: false,
      superEntriesLoaded: false,
      updateCount: 0,
      camouflageCount: null,
      consumableCount: null,
      currencyCount: null,
      flagCount: null,
      specialCount: null,
      camouflageCountNormal: null,
      consumableCountNormal: null,
      currencyCountNormal: null,
      flagCountNormal: null,
      specialCountNormal: null,
      camouflageCountSuper: null,
      consumableCountSuper: null,
      currencyCountSuper: null,
      flagCountSuper: null,
      specialCountSuper: null
    };
  }

  componentDidUpdate(){
    {this.props.update(this.state.specialCount)}
  }

  getAllEntries(){
    let dis = this;
    if(this.state.allEntriesLoaded===false){
      dis.setState({allEntriesLoaded:true});
      let suffix = "getAllUserEntries/"+this.state.uid;
      if(dis.state.camouflageCount===null){
        fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
        .then(response => response.json())
        .then(function(json){
          try{
            Object.entries(json.entries).forEach(cat => {
              let result = 0;
              switch(cat[0]){
                case "camouflageCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({camouflageCount: result});
                  break;
                case "consumableCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({consumableCount: result});
                  break;
                case "currencyCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({currencyCount:  result});
                  break;
                case "flagCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({flagCount:  result});
                  break;
                case "specialCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({specialCount: result});
                  break;
              }
            });

          }catch(e){
            throw new Error(e);
          }
          dis.setState({loaded: true});
        });
      }
    }
  }

  
  getNormalEntries(){
    let dis = this;
    if(this.state.normalEntriesLoaded===false){
      dis.setState({normalEntriesLoaded:true});
      let suffix = "getNormalUserEntries/"+this.state.uid;
      if(dis.state.camouflageCount===null){
        fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
        .then(response => response.json())
        .then(function(json){
          try{
            Object.entries(json.entries).forEach(cat => {
              let result = 0;
              switch(cat[0]){
                case "camouflageCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({camouflageCountNormal: result});
                  break;
                case "consumableCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({consumableCountNormal: result});
                  break;
                case "currencyCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({currencyCountNormal:  result});
                  break;
                case "flagCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({flagCountNormal:  result});
                  break;
                case "specialCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({specialCountNormal: result});
                  break;
              }
            });

          }catch(e){
            throw new Error(e);
          }
          dis.setState({loaded: true});
        });
      }
    }
  }
  
  getSuperEntries(){
    let dis = this;
    if(this.state.superEntriesLoaded===false){
      dis.setState({superEntriesLoaded:true});
      let suffix = "getSuperUserEntries/"+this.state.uid;
      if(dis.state.camouflageCount===null){
        fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
        .then(response => response.json())
        .then(function(json){
          try{
            Object.entries(json.entries).forEach(cat => {
              let result = 0;
              switch(cat[0]){
                case "camouflageCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({camouflageCountSuper: result});
                  break;
                case "consumableCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({consumableCountSuper: result});
                  break;
                case "currencyCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({currencyCountSuper:  result});
                  break;
                case "flagCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({flagCountSuper:  result});
                  break;
                case "specialCount":
                  Object.entries(cat[1]).forEach(entry => {
                    result += entry[1].amount;
                  });
                  dis.setState({specialCountSuper: result});
                  break;
              }
            });

          }catch(e){
            throw new Error(e);
          }
          dis.setState({loaded: true});
        });
      }
    }
  }
  
  lcfirst(string){
    return string.charAt(0).toLowerCase() + string.slice(1);
  }

  getStats(){
    if(this.state.uid){
      let dis = this;
      this.getNormalEntries();
      this.getSuperEntries();
      this.getAllEntries();
      let types = ["Normal","Super","All"];
      let el = null;
      let camo = null;
      let consum = null;
      let curr = null;
      let flag = null;
      let spec = null;

      types.forEach(async contType => {
        let all = "";
        switch(contType){
          case "Normal":
            camo = dis.state.camouflageCountNormal;
            consum = dis.state.consumableCountNormal;
            curr = dis.state.currencyCountNormal;
            flag = dis.state.flagCountNormal;
            spec = dis.state.specialCountNormal;
            all = " normalAmountNumber";
            break;
          case "Super":
            camo = dis.state.camouflageCountSuper;
            consum = dis.state.consumableCountSuper;
            curr = dis.state.currencyCountSuper;
            flag = dis.state.flagCountSuper;
            spec = dis.state.specialCountSuper;
            all = " superAmountNumber";
            break;
          case "All":
            camo = dis.state.camouflageCount;
            consum = dis.state.consumableCount;
            curr = dis.state.currencyCount;
            flag = dis.state.flagCount;
            spec = dis.state.specialCount;
            all = " amountNumber";
            break;
          default:
            break;
        }
        if(el===null){
          el = "<b>"+contType+" container amounts</b><br />Camouflages <span id='"+this.lcfirst(contType)+"_camouflages_amounts' class='floatRight"+all+"'>"+camo+"</span><br />" +
          "Consumables <span id='"+this.lcfirst(contType)+"_consumables_amounts' class='floatRight"+all+"'>"+consum+"</span><br />" +
          "Currency <span id='"+this.lcfirst(contType)+"_currency_amounts' class='floatRight"+all+"'>"+curr+"</span><br />" +
          "Flags <span id='"+this.lcfirst(contType)+"_flags_amounts' class='floatRight"+all+"'>"+flag+"</span><br />" +
          "Specials <span id='"+this.lcfirst(contType)+"_specials_amounts' class='floatRight"+all+"'>"+spec+"</span><br /><br />";
        }else{
          el += "<b>"+contType+" container amounts</b><br />Camouflages <span id='"+this.lcfirst(contType)+"_camouflages_amounts' class='floatRight"+all+"'>"+camo+"</span><br />" +
          "Consumables <span id='"+this.lcfirst(contType)+"_consumables_amounts' class='floatRight"+all+"'>"+consum+"</span><br />" +
          "Currency <span id='"+this.lcfirst(contType)+"_currency_amounts' class='floatRight"+all+"'>"+curr+"</span><br />" +
          "Flags <span id='"+this.lcfirst(contType)+"_flags_amounts' class='floatRight"+all+"'>"+flag+"</span><br />" +
          "Specials <span id='"+this.lcfirst(contType)+"_specials_amounts' class='floatRight"+all+"'>"+spec+"</span><br /><br />";
        }
      });
      // dis.props.update();
      return(
        <div className="stats">
          <p>Container amounts for user <b>{dis.state.uname}</b></p>
          <br />
          <div dangerouslySetInnerHTML={{__html: el}}></div>
        </div>
      )
    }
  }

  render() {
    // camouflageCount: null,
    // consumableCount: null,
    // currencyCount: null,
    // flagCount: null,
    // specialCount: null,
    // camouflageCountNormal: null,
    // consumableCountNormal: null,
    // currencyCountNormal: null,
    // flagCountNormal: null,
    // specialCountNormal: null,
    // camouflageCountSuper: null,
    // consumableCountSuper: null,
    // currencyCountSuper: null,
    // flagCountSuper: null,
    // specialCountSuper: null
    return (
      <div className="pageRight pageSides">
        {this.getStats()}
      </div>
    );
  }
}

export default Right;
