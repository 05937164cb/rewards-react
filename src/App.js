import React, { Component } from 'react';
import "./css/wows_numbers.css";
import "./css/dd.css";
import "./css/font-awesome.css";
import Left from "./left";
import Right from "./right";
import Config from "./config.js";

class Content extends Component {

  constructor(props){
    super(props);
    this.state = {
                    loaded: false,
                    updated: false,
                    typeByIdLoaded: false,
                    amountsLoaded: false,
                    data: null,
                    type: null,
                    typeId: this.props.match.params.type,
                    loggedIn: false,
                    uname: null,
                    all: null,
                    allNormal: null,
                    allTypesValue: null,
                    normalTypesValue: null,
                    superTypesValue: null,
                    allSuper: null,
                    allTypesValueLoaded: false
                  };
    this.doUpdate = this.doUpdate.bind(this);
    this.getTypeById = this.getTypeById.bind(this);
  }

  componentDidMount(){
    // this.setState({loaded: false});
    let params = this.props.match.params;
    let id = params.id;
    let supr = params.supr;
    let type = params.type;
    let dis = this;
    if(id){
      let suffix = "user/"+id;
      if(supr && type){
        suffix += "/"+supr+"/"+type;
      }
      fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
      .then(response => response.json())
      .then(function(json){
        dis.setState({data: json});
        dis.setState({uname: json.uname});
        dis.setUname(json);
        // dis.setState({loaded: true});
        }
      )

      suffix="getTypeById/"+type;
      fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
      .then(response => response.json())
      .then(function(json){
        dis.setState({type: json.name});
        // dis.setState({loaded: true});
      })
      let contType = this.state.type;
      let allTypes = document.getElementById("all_"+contType+"_amounts");
      dis.setState({allTypesValue: allTypes});
      // document.getElementById("all_"+contType+"_amounts");

    }else{
      dis.setState({loaded: true});
    }
    dis.setState({loaded:true});
  }

  componentDidUpdate(){
    let dis = this;
    let type = this.props.match.params.type;
    let suffix="getTypeById/"+type;
    if(dis.state.allTypesValueLoaded===false){
      fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
      .then(response => response.json())
      .then(function(json){
        dis.setState({type: json.name});
        // dis.setState({loaded: true});
      })
      let contType = this.state.type;
      let allTypes = document.getElementById("all_"+contType+"_amounts");
      dis.setState({allTypesValue: allTypes});
      dis.setState({allTypesValueLoaded: true});
    }
    // if(this.state.loaded===true){
    //   this.doUpdate();
    // }
  }

  setUname(json){
    this.setState({uname: json.uname});
  }

  getUserContent(){
    let params = this.props.match.params;
    let id = params.id;
    let supr = params.supr;
    let type = params.type;
    let loaded = this;
    if(id){
      // if(loaded.state.data==="null"){
      //   let suffix="getTypeById/"+type;
      //   fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
      //   .then(response => response.json())
      //   .then(function(json){
      //     console.log(json);
      //     console.log("TYPEBYID: "+json);
      //     // contType=json.name
      //   })
      // }
      let contType=null;
      // if(loaded.state.loaded===false && loaded.state.type===null){
      //   let suffix="getTypeById/"+type;
      //   fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
      //   .then(response => response.json())
      //   .then(function(json){
      //     loaded.setState({type: json.name});
      //     loaded.setState({loaded: true});
      //   })
      // }

      if(this.state.data){
        let rarity=null;
        console.log(supr);
        if(supr==="1"){
          rarity = "super container";
        }else{
          rarity = "normal container";
        }

        contType = this.state.type;
        let allTypes = document.getElementById("all_"+contType+"_amounts");
        if(this.state.allTypesValue!=="null"){
          try{
            if(allTypes && typeof allTypes !== 'undefined' && allTypes.innerText!=="null"){
              // loaded.setState({allTypesValue: allTypes.innerText});
            }
          }catch(e){
            console.log("Trying to get all values...");
            throw new Error(e);
            loaded.setState({loaded:false});
            // alert("all_"+contType+"_amounts");
          }
        }
        let rows = "";
        return(
            <div className="rows">
              <div className='hidden' id='tableType'>{contType}</div>
              <table style={{clear: 'both', width: "100%"}}>
                <tr className='row tableHeader'>
                  <td>Entry name</td>
                  <td>Amount</td>
                  <td>% of all {contType}</td>
                  <td>% of all {rarity} {contType}</td>
                  <td>% of all rewards</td>
                  <td>% of {rarity} rewards</td>
                  <td>Actions</td>
                </tr>
                {this.state.data.result.map(function(obj){
                  let icon = null;
                  obj["icon"].map(function(icons){
                    icon = require("./images/reward_icons/"+icons["filename"]);
                  })
                  let ofSuperType = "";
                  let ofNormalType = "";
                  if(supr==="1"){
                    ofSuperType = loaded.state.superTypesValue;
                    ofNormalType = loaded.state.allSuper;
                  }else{
                    ofSuperType = loaded.state.normalTypesValue;
                    ofNormalType = loaded.state.allNormal;
                  }
                    return(
                      <tr>
                        <td><img style={{height: "40px"}} src={icon} />{obj.name}</td>
                        <td>{obj.amount}</td>
                        <td className="ofType">{Number((obj.amount / loaded.state.allTypesValue) * 100).toFixed(4)}%</td>
                        <td className="ofSuperType">{Number((obj.amount / ofSuperType) * 100).toFixed(4)}%</td>
                        <td className="ofAll">{Number((obj.amount / loaded.state.all) * 100).toFixed(4)}%</td>
                        <td className="ofNormalType">{Number((obj.amount / ofNormalType) * 100).toFixed(4)}%</td>
                        <td className="actions"></td>
                      </tr>
                    )
                })}
              </table>
            </div>
          );
      }else{
        return(null);
      }
      
    }else{
      return("");
    }
  }

  getMain(){
    var cont = "Hello stranger.";
    return(cont);
  }

  getContent(){
    if(this.state.data){
      let user = this.state.data.uname;
      let camoCount = this.state.data.camouflageCount;
      let consCount = this.state.data.consumableCount;
      let currCount = this.state.data.currencyCount;
      let flagCount = this.state.data.flagCount;
      let specCount = this.state.data.specialCount;
      return(
        <div className="content">
          <div style={{clear: "both"}}>
            <p><b>User stats for user {user}</b></p>
          </div>
          <div style={{clear: "both"}}>
            {this.getUserContent()}
          </div>
        </div>
      );
    }else{
      let margin = "50px";
      return(
          <div className="content" style={{marginTop: margin}}>
            {this.getMain()}<br />
          </div>
      );
    }
  }

  getTypeById(id){
    if(this.state.typeByIdLoaded===false){
      // this.setState({loaded: true});
      this.setState({typeByIdLoaded:true});
      let suffix="getTypeById/"+id;
      let dis = this;
      let rtrn = null;
      fetch("http://api.wows.rule10.eu/api/v1/"+suffix)
      .then(response => response.json())
      .then(function(json){
        dis.setState({typeId: json.name});
        rtrn = json.name
      })
      return(rtrn)
    }
  }

  lcfirst(string){
    return string.charAt(0).toLowerCase() + string.slice(1);
  }

  doUpdate(val){
    let allTypes;
    let normalTypes;
    let superTypes;
    let all = 0;
    let allNormal = 0;
    let allSuper = 0;
    let contType = "";
    if(typeof this.state.type !== "string"){
      contType = this.getTypeById(this.state.type);
    }else{
      contType = this.state.type;
    }
    // if(this.state.loaded===false){

      // normalAmountNumber

      try{
        let elems = document.getElementsByClassName("amountNumber");
        let normalElems = document.getElementsByClassName("normalAmountNumber");
        let superElems = document.getElementsByClassName("superAmountNumber");
        Array.prototype.forEach.call(elems, function(el){
          all = all + parseInt(el.innerText);
        });
        Array.prototype.forEach.call(normalElems, function(el){
          allNormal = allNormal + parseInt(el.innerText);
        });
        Array.prototype.forEach.call(superElems, function(el){
          allSuper = allSuper + parseInt(el.innerText);
        });

        allTypes = document.getElementById("all_"+contType+"_amounts").innerText;
        normalTypes = document.getElementById("normal_"+contType+"_amounts").innerText;
        superTypes = document.getElementById("super_"+contType+"_amounts").innerText;
      }catch(e){
        console.log("all_"+contType+"_amounts");
      }
      if(allTypes!=="null"){
        // let camoCount = this.state.data.camouflageCount;
        // let consCount = this.state.data.consumableCount;
        // let currCount = this.state.data.currencyCount;
        // let flagCount = this.state.data.flagCount;
        // let specCount = this.state.data.specialCount;
        // let consCount = consCount + consum;
          if(this.state.updated===false && typeof val==="number"){
            this.setState({loaded: true});
            console.log("CT: "+contType);
            try{
              // or allTypes
              this.setState({all: all});
              this.setState({allNormal: allNormal});
              this.setState({allTypesValue: allTypes});
              this.setState({normalTypesValue: normalTypes});
              this.setState({superTypesValue: superTypes});
              this.setState({allSuper: allSuper});
              // this.setState({camouflageCount: camo});
            }catch(e){
              console.log("could not do update.");
              throw new Error(e);
            }
            this.setState({updated: true});
          }
      }
    // }
  }

  render() {
    if(this.state.loaded === false){
      return(
        <div className="fullPage">
          <p className="loader">
            <b>Loading...</b><br /><br />
            <i class="fa fa-spinner fa-pulse fa-4x"></i>
          </p>
        </div>
      );
    }else{
      return (
        <div>
          <Left uid={this.props.match.params.id} contType={this.props.match.params.type} superType={this.props.match.params.supr} />
          {this.getContent()}
          <Right update={this.doUpdate} uid={this.props.match.params.id} uname={this.state.uname} />
        </div>
      );
    }
  }
}

export default Content;
